#!/bin/bash

if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Please run as root or sudo"
    exit
fi

set -o nounset
set -o pipefail

#safe script path first
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#get project name

read -e -p 'Project Name: ' projectName
read -e -p 'Custom Domain: ' customDomain

#make directory for project

cd /var/www/sites
folderName="$projectName".quickqueue.com.my
#mkdir "$folderName"
#cd "$folderName"

#clone repo

repo='git@bitbucket.org:dingyonglaw/quick-queue-web-system.git'

sudo -u QuickQueue git clone "$repo" "$folderName"

echo '-----------------------------------------------------------'
echo '---------Changing Folder Permission For UI Conf------------'
echo '-----------------------------------------------------------'

chown -R www-data:QuickQueue /var/www/sites/"$projectName".quickqueue.com.my/sites/all/modules/quickqueue_event/datasource/interface/templates/

#create mysql database
databaseName=qq_"$projectName"
databaseWithSlash="${databaseName//[^a-zA-Z0-9]/_}"

rootPassword=QQadmin@1!

#mysql -u root -p  -e "CREATE DATABASE "$databaseWithSlash" CHARACTER SET utf8 COLLATE utf8_general_ci"
#mysql -u root -p  -e "USE "$databaseWithSlash""
#mysql -u root -p  -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, LOCK TABLES, CREATE TEMPORARY TABLES ON "$databaseWithSlash" TO 'root'@'localhost' IDENTIFIED BY '"rootPassword"'"

#copy files & change folder permission for apache
cp /var/www/sites/"$folderName"/sites/default/default.settings.php /var/www/sites/"$folderName"/sites/default/settings.php
chown www-data /var/www/sites/"$folderName"/sites/default/settings.php
chown -R www-data /var/www/sites/"$folderName"/sites/default/files

#install drush minimal
echo '-----------------------------------'
echo '---------drush installation--------'
echo '-----------------------------------'

drushDummy="dummy"
databaseBackup="/var/www/sites/"$folderName"/backups/qq_base_backup.sql"

cd /var/www/sites/"$folderName"
/home/QuickQueue/.config/composer/vendor/bin/drush site-install minimal --account-name="$drushDummy" --account-pass="$drushDummy" --db-url=mysql://root:"$rootPassword"@localhost/"$databaseWithSlash"

echo '----------Installing composer libs---------'
cd /var/www/sites/"$folderName"/sites/all/modules/quickqueue_event
sudo -u QuickQueue composer install

echo '---------------Restoring DB--------------'

mysql -u root -p"$rootPassword" $databaseWithSlash < $databaseBackup

echo '---------------Site Setup Done--------------'
echo '---------Setting Up SubDomain---------------'

#go to script directory

cd "$SCRIPTDIR"
pwd

# setup qq subdomain
defaultConf=default.conf
subdomainFileName="$projectName".quickqueue.com.my.conf
cp "$defaultConf" "$subdomainFileName"
sed -i -e "s/{{{projectName}}}/${projectName}/g" ${subdomainFileName}
mv ./"$subdomainFileName" /etc/apache2/sites-available/
a2ensite "$subdomainFileName"
service apache2 reload
echo '#$base_url = '"'"'https://'"$projectName"'.quickqueue.com.my'"';" >> /var/www/sites/"$folderName"/sites/default/settings.php

curl -X POST "https://api.cloudflare.com/client/v4/zones/6075dad0390a8dbbbfb20102d9261f58/dns_records" \
     -H "X-Auth-Email: admin@codesocialist.com" \
     -H "X-Auth-Key: 90d3c0f12336e988d77a070c3c62b0875f358" \
     -H "Content-Type: application/json" \
     --data '{"type":"CNAME","name":"'"$projectName"'.quickqueue.com.my","content":"quickqueue.com.my","ttl":1,"proxied":false}'

# setup custom domain
customConf=custom.conf
customDomainFileName="$customDomain".conf
cp "$customConf" "$customDomainFileName"
sed -i -e "s/{{{projectName}}}/${projectName}/g" ${customDomainFileName}
sed -i -e "s/{{{customDomain}}}/${customDomain}/g" ${customDomainFileName}
mv ./"$customDomainFileName" /etc/apache2/sites-available/
a2ensite "$customDomainFileName"
service apache2 reload
echo '$base_url = '"'"'https://'"$customDomain"''"';" >> /var/www/sites/"$folderName"/sites/default/settings.php


#Set up crontab for email & sms queue
NEW_UUID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
echo '$conf['"'"'cron_key'"'"'] = '"'"$NEW_UUID"';" >> /var/www/sites/"$folderName"/sites/default/settings.php

(crontab -l ; echo '#'"$projectName") | crontab -
(crontab -l ; echo '* * * * * wget -O - -q -t 1 https://'"$projectName"'.quickqueue.com.my/cron.php?cron_key='"$NEW_UUID") | crontab -



echo '-----------------------------------'
echo '---- Setting Up SSL QQ Domain -----'
echo '-----------------------------------'
certbot --apache -d "$projectName".quickqueue.com.my


echo '-----------------------------------'
echo '-- Setting Up SSL Custom Domain ---'
echo '-----------------------------------'
certbot --apache -d "$customDomain"