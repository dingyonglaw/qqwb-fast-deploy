#!/bin/bash

isDirectory=false

#get local directory for setup
while [ "$isDirectory" = 'false' ]; do
  if [ ! -d "$dirToSetup" ]; then
    read -e -p 'Set Up Directory: ' dirToSetup
  else
    break 
  fi
done

read -p 'New Folder Name: ' folderName

#make directory if it doesnt exist
dirToSetup=$dirToSetup$folderName
mkdir -p $dirToSetup

#cloning repo
read -p 'Bitbucket username: ' bitbucketUsername

repo='https://'$bitbucketUsername'@bitbucket.org/dingyonglaw/quick-queue-web-system.git'

git clone "$repo" "$dirToSetup"

#create mysql database
read -p 'New Database Name: ' databaseName
read -sp 'root password(blank for none): ' rootPassword

#put slashes in front of underscore
replacingString=\_
databaseWithSlash="${databaseName//_/$replacingString}"

if [ -z "$rootPassword" ]; then
  printf "\nContinuing without password\n"
  mysql -u root -e "CREATE DATABASE "$databaseWithSlash" CHARACTER SET utf8 COLLATE utf8_general_ci"
  mysql -u root -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, LOCK TABLES, CREATE TEMPORARY TABLES ON "$databaseWithSlash" TO 'root'@'localhost'" 
else
  printf "\ncontinuing with password\nUse your root password below for database transactions\n"
  mysql -u root -p -e "CREATE DATABASE "$databaseWithSlash" CHARACTER SET utf8 COLLATE utf8_general_ci"
  mysql -u root -p -e "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, LOCK TABLES, CREATE TEMPORARY TABLES ON "$databaseWithSlash".* TO 'root'@'localhost' IDENTIFIED BY '"$rootPassword"'" 
fi

#copy files & change folder permission to make accesible to apache
cp $dirToSetup/sites/default/default.settings.php $dirToSetup/sites/default/settings.php
sudo chown www-data:www-data $dirToSetup/sites/default/settings.php
sudo chown -R www-data:www-data $dirToSetup/sites/default/files

#go to directory
cd $dirToSetup

#install drush minimal
echo '---------------------------------'
echo '--------drush installation-------'
echo '---------------------------------'

read -p 'Account Name: ' drushAccName
read -p 'Account Password: ' drushAccPassword

isFile=false

while [ "$isFile" = 'false' ]; do
  if [ ! -e "$databaseBackup" ]; then
    read -e -p 'Database Backup File: ' databaseBackup
  else
    break
  fi
done

if [ -z "$rootPassword" ]; then
  drush site-install minimal --account-name="$drushAccName" --account-pass="$drushAccPassword" --db-url=mysql://root@localhost/"$databaseWithSlash"
else
  drush site-install minimal --account-name="$drushAccName" --account-pass="$drushAccPassword" --db-url=mysql://root:"$rootPassword"@localhost/"$databaseWithSlash"
fi

drush en entity_reference
drush en backup_migrate

if [ -z "$rootPassword" ]; then
  mysql -u root $databaseName < $databaseBackup
else
  mysql -u root -p"$rootPassword" $databaseName < $databaseBackup
fi

echo '----------Setup Done-------------' 
